package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.pool2.BaseObject;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

import static org.apache.camel.model.dataformat.JsonLibrary.Gson;

@Component
public class Request extends BaseObject implements Serializable {

    private Identity identity;

    private BaseParameter parameter;

    ObjectMapper oMapper = new ObjectMapper();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Paging paging;

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public BaseParameter getParameter() {
        return parameter;
    }

    public void setParameter(BaseParameter parameter) {
        this.parameter = parameter;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

}
