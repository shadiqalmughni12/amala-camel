package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

public class Paging implements Serializable {

    private int page;

    private int limit;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int totalpages;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private long total;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(int totalpages) {
        this.totalpages = totalpages;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

}
