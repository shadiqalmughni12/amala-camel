package id.co.asyst.amala.camel.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;

public class Identity implements Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reqtxnid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reqdate;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String apptxnid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String appid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String userid;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String signature;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String seqno;

    public String getReqtxnid() {
        return reqtxnid;
    }

    public void setReqtxnid(String reqtxnid) {
        this.reqtxnid = reqtxnid;
    }

    public String getReqdate() {
        return reqdate;
    }

    public void setReqdate(String reqdate) {
        this.reqdate = reqdate;
    }

    public String getApptxnid() {
        return apptxnid;
    }

    public void setApptxnid(String apptxnid) {
        this.apptxnid = apptxnid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSeqno() {
        return seqno;
    }

    public void setSeqno(String seqno) {
        this.seqno = seqno;
    }
}
